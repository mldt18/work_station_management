package com.alliance.controller.view;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alliance.utility.DataPackager;

@Controller
@RequestMapping("/admin")
public class AdminViewController {
	
	@RequestMapping( value="/login", method = RequestMethod.GET)
	public String adminLogin(HttpServletRequest request) {
		return "admin/login"; 
	}
	
	@RequestMapping( value="/dashboard", method = RequestMethod.GET)
	public String adminView(HttpServletRequest request) {
		return "admin/dashboard"; 
	}
	
	@RequestMapping( value="/updateAdmin", method = RequestMethod.GET)
	public String updateAdmin(HttpServletRequest request) {
		return "admin/update"; 
	}
	
	@RequestMapping( value="/addAdmin", method = RequestMethod.GET)
	public String addAdmin(HttpServletRequest request) {
		return "admin/addAdmin"; 
	}
}

